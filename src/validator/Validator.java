package validator;

public class Validator {
    public static boolean isStringValid(String parameter) {
        boolean isStringNotNullOrEmpty = parameter != null && !parameter.isEmpty() && !parameter.trim().isEmpty();

        return isStringNotNullOrEmpty;
    }

    public static boolean isNumberInInterval(int number, int lowerBound, int upperBound) {
        boolean isNumberInInterval = lowerBound <= number && number <= upperBound;

        return isNumberInInterval;
    }

    public static boolean isNumberInInterval(long number, long lowerBound, long upperBound) {
        boolean isNumberInInterval = lowerBound <= number && number <= upperBound;

        return isNumberInInterval;
    }

    public static boolean isNumberInInterval(double number, double lowerBound, double upperBound) {
        boolean isNumberInInterval = lowerBound <= number && number <= upperBound;

        return isNumberInInterval;
    }

    public static boolean isPositive(double number) {
        return number >= 0;
    }
}
