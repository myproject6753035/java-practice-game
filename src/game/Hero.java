package game;

import validator.Validator;

public class Hero {
    private final static byte INITIAL_LEVEL = 1;
    private final static byte MAX_LEVEL = 100;
    private final static int INITIAL_HEALTH = 1;
    private final static int INITIAL_DAMAGE_REDUCTION = 1;
    private final static int INITIAL_DAMAGE = 1;
    private final static int HEALTH_MULTIPLIER = 30;

    private String name;
    private HeroClass heroClass;
    private byte level;
    private long health;
    private Armor head;
    private Armor shoulders;
    private Armor torso;
    private Armor gloves;
    private Armor pants;
    private Armor boots;
    private Armor ring;
    private Weapon weapon;
    private long totalDamageReduction;
    private long totalDamage;

    public Hero(String name, HeroClass heroClass) {
        this.setName(name);
        this.heroClass = heroClass;
        this.level = INITIAL_LEVEL;
        this.health = INITIAL_HEALTH;
        this.totalDamageReduction = INITIAL_DAMAGE_REDUCTION;
        this.totalDamage = INITIAL_DAMAGE;
    }

    public Hero(String name, HeroClass heroClass, byte level) {
        this(name, heroClass);
        this.setLevel(level);
    }

    public String getName() {
        return this.name;
    }

    private void setName(String name) {
        boolean isParameterValid = Validator.isStringValid(name);

        if (!isParameterValid) {
            throw new IllegalArgumentException("Invalid name!");
        }

        this.name = name;
    }

    public HeroClass getHeroClass() {
        return this.heroClass;
    }

    public byte getLevel() {
        return this.level;
    }

    public void increaseLevel() {
        if (this.level == MAX_LEVEL) {
            return;
        }

        this.level++;
    }

    public long getHealth() {
        return this.health;
    }

    public void equip(Armor armor) {
        ArmorType armorType = armor.getArmorType();

        switch (armorType) {
            case Head:
                if (this.head != null) {
                    this.totalDamageReduction -= this.head.getDamageReduction();
                }

                this.head = armor;
                break;
            case Shoulders:
                if (this.shoulders != null) {
                    this.totalDamageReduction -= this.shoulders.getDamageReduction();
                }

                this.shoulders = armor;
                break;
            case Torso:
                if (this.torso != null) {
                    this.totalDamageReduction -= this.torso.getDamageReduction();
                }

                this.torso = armor;
                break;
            case Gloves:
                if (this.gloves != null) {
                    this.totalDamageReduction -= this.gloves.getDamageReduction();
                }

                this.gloves = armor;
                break;
            case Pants:
                if (this.pants != null) {
                    this.totalDamageReduction -= this.pants.getDamageReduction();
                }

                this.pants = armor;
                break;
            case Boots:
                if (this.boots != null) {
                    this.totalDamageReduction -= this.boots.getDamageReduction();
                }

                this.boots = armor;
                break;
            case Ring:
                if (this.ring != null) {
                    this.totalDamageReduction -= this.ring.getDamageReduction();
                }

                this.ring = armor;
                break;
        }

        this.totalDamageReduction += armor.getDamageReduction();
        long healthToAdd = (long)armor.getDamageReduction() * HEALTH_MULTIPLIER;
        this.increaseHealth(healthToAdd);
    }

    public void equip(Weapon weapon) {
        if (this.weapon != null) {
            this.totalDamage -= this.weapon.getDamage();
        }

        this.weapon = weapon;
        this.totalDamage += weapon.getDamage();
    }

    public Weapon getWeapon() {
        return this.weapon;
    }

    public Armor getArmor(ArmorType armorType) {
        switch (armorType) {
            case Head:
                return this.head;
            case Shoulders:
                return this.shoulders;
            case Torso:
                return this.torso;
            case Gloves:
                return this.gloves;
            case Pants:
                return this.pants;
            case Boots:
                return this.boots;
            case Ring:
                return this.ring;
            default:
                throw new IllegalArgumentException("Invalid armor type!");
        }
    }

    public boolean isAlive() {
        return this.getHealth() != 0;
    }

    public void doDamage(Hero otherHero) {
        long damageDone = this.totalDamage - otherHero.totalDamageReduction;
        otherHero.decreaseHealth(damageDone);
    }

    private void increaseHealth(long health) {
        boolean isParameterValid = Validator.isPositive(health);

        if (!isParameterValid) {
            throw new IllegalArgumentException("Invalid parameter!");
        }

        this.health += health;
    }

    private void decreaseHealth(long health) {
        boolean isParameterValid = Validator.isPositive(health);

        if (!isParameterValid) {
            throw new IllegalArgumentException("Invalid parameter!");
        }

        if (this.getHealth() - health <= 0) {
            this.health = 0;
            return;
        }

        this.health -= health;
    }

    private void setLevel(byte level) {
        boolean isParameterValid = Validator.isNumberInInterval(level, INITIAL_LEVEL, MAX_LEVEL);

        if (!isParameterValid) {
            throw new IllegalArgumentException("Invalid level!");
        }

        this.level = level;
    }
}
