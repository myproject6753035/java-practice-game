package game;

public enum ArmorType {
    Head,
    Shoulders,
    Torso,
    Gloves,
    Pants,
    Boots,
    Ring
}
