package game;

import validator.Validator;

public abstract class Item {
    private static final short MIN_ITEM_LEVEL = 1;
    private static final short MAX_ITEM_LEVEL = 240;

    private String name;
    private short itemLevel;
    private long copperPrice;

    public Item(String name, short itemLevel, long copperPrice) {
        this.setName(name);
        this.setItemLevel(itemLevel);
        this.setCopperPrice(copperPrice);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        boolean isParameterValid = Validator.isStringValid(name);

        if (!isParameterValid) {
            throw new IllegalArgumentException("Invalid name!");
        }

        this.name = name;
    }

    public short getItemLevel() {
        return this.itemLevel;
    }

    public void setItemLevel(short itemLevel) {
        boolean isParameterValid = Validator.isNumberInInterval(itemLevel, MIN_ITEM_LEVEL, MAX_ITEM_LEVEL);

        if (!isParameterValid) {
            throw new IllegalArgumentException("Invalid item level!");
        }

        this.itemLevel = itemLevel;
    }

    public long getCopperPrice() {
        return this.copperPrice;
    }

    public void setCopperPrice(long copperPrice) {
        boolean isParameterValid = Validator.isPositive(copperPrice);

        if (!isParameterValid) {
            throw new IllegalArgumentException("Invalid price!");
        }

        this.copperPrice = copperPrice;
    }
}
