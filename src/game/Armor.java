package game;

public class Armor extends Item {
    private final static int ITEM_LEVEL_MULTIPLIER = 5;

    private final ArmorType armorType;

    public Armor(String name, short itemLevel, long copperPrice, ArmorType armorType) {
        super(name, itemLevel, copperPrice);
        this.armorType = armorType;
    }

    public ArmorType getArmorType() {
        return this.armorType;
    }

    public int getDamageReduction() {
        return this.getItemLevel() * ITEM_LEVEL_MULTIPLIER;
    }
}
