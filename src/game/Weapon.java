package game;

public class Weapon extends Item {
    private final static int ITEM_LEVEL_MULTIPLIER = 50;

    private final WeaponType weaponType;

    public Weapon(String name, short itemLevel, long copperPrice, WeaponType weaponType) {
        super(name, itemLevel, copperPrice);
        this.weaponType = weaponType;
    }

    public WeaponType getWeaponType() {
        return this.weaponType;
    }

    public int getDamage() {
        return this.getItemLevel() * ITEM_LEVEL_MULTIPLIER;
    }
}
