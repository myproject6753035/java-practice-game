package game;

public enum WeaponType {
    Sword,
    Staff,
    Axe,
    Mace,
    Bow,
    Dagger
}
