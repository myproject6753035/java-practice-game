package game;

public enum HeroClass {
    Warrior,
    Paladin,
    Hunter,
    Mage,
    Shaman,
    Druid
}
