import game.*;

public class Main {
    public static void main(String[] args) {
        // Creating the first player
        // --------------------------------------------------------------------------
        Armor[] firstPlayerArmors = createArmors(); // creates a full set of armors for the first player
        Weapon firstPlayerWeapon = new Weapon("The Purple Axe of Lord Marvin", (short)145, 4385325235L, WeaponType.Axe);
        Hero firstPlayer = new Hero("Uther the Lightbringer", HeroClass.Paladin, (byte)90);

        for (int index = 0; index < firstPlayerArmors.length; index++) {
            Armor currentArmor = firstPlayerArmors[index];
            firstPlayer.equip(currentArmor);
        }

        firstPlayer.equip(firstPlayerWeapon);
        // --------------------------------------------------------------------------

        // Creating the second player
        // --------------------------------------------------------------------------
        Armor[] secondPlayerArmors = createArmors(); // creates a full set of armors for the second player
        Weapon secondPlayerWeapon = new Weapon("Thunderfury, Blessed Blade of the Windseeker", (short)146, 6483743923L, WeaponType.Sword);
        Hero secondPlayer = new Hero("Garrosh Hellscream", HeroClass.Warrior, (byte)90);

        for (int index = 0; index < secondPlayerArmors.length; index++) {
            Armor currentArmor = secondPlayerArmors[index];
            secondPlayer.equip(currentArmor);
        }

        secondPlayer.equip(secondPlayerWeapon);
        // --------------------------------------------------------------------------

        while (firstPlayer.isAlive() && secondPlayer.isAlive()) {
            firstPlayer.doDamage(secondPlayer);
            secondPlayer.doDamage(firstPlayer);
        }

        String winnerName = "";

        if (firstPlayer.isAlive()) {
            winnerName = firstPlayer.getName();
        } else if (secondPlayer.isAlive()) {
            winnerName = secondPlayer.getName();
        }

        System.out.printf("The winner is: %s!", winnerName);
    }

    private static Armor[] createArmors() {
        ArmorType[] armorTypes = ArmorType.values(); // gets all ArmorType elements as an array of type ArmorType
        int count = armorTypes.length; // gets the count of elements of the ArmorType[]
        Armor[] armors = new Armor[count];

        for (int index = 0; index < count; index++) {
            String name = armorTypes[index].toString(); // gets the string representation of the current ArmorType[] element
            int itemLevel = 99 + (int)Math.ceil(Math.random() * 101); // gets a random number between 100 and 200
            ArmorType armorType = armorTypes[index]; // gets the current ArmorType[] element
            Armor armor = new Armor(name, (short)itemLevel, 0L, armorType); // creates a new instance of the Armor class
            armors[index] = armor; // adds the instance to the Armor[]
        }

        return armors;
    }
}
